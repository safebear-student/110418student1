pipeline {
    options {
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }

    agent any
    parameters {
        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API tests')
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')
        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://54.191.126.240', description: 'domain of the test environment')
        string(name: 'test_hostname', defaultValue: '54.191.126.240', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')

    }

    triggers { pollSCM('* * * * *')}

    stages {
        stage('Build with unit testing'){
            steps{
                sh 'mvn clean package'
            }
            post {
                success {
                    echo 'Now Archiving...'


                    archiveArtifacts artifacts: '**/target/*.war'
                }
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }

        }
        stage('Static Analysis'){
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                }
            }

        }
        stage('deploy to Test') {
            steps {
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
        }

        stage('integration tests'){
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }

        }

        stage('cucumber bdd tests'){

            steps{

                sh "mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always{
                    publishHTML([
                            allowMissing            : false,
                            alwaysLinkToLastBuild   : false,
                            keepAll                 : false,
                            reportDir               : 'target/cucumber',
                            reportFiles             : 'index.html',
                            reportName              : 'BDD report',
                            reportTitles            : ''
                    ])
                }
            }

        }
    }
}