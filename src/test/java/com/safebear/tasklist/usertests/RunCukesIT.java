package com.safebear.tasklist.usertests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by cca_student on 11/04/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        tags = "~@to-do",
        glue = "com.safebear.tasklist.usertests",
        features = "classpath:tasklist.features/taskmanagement.feature"
)
public class RunCukesIT {
}
