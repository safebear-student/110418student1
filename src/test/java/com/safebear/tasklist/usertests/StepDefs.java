package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by cca_student on 11/04/2018.
 */
public class StepDefs {

    TaskListPage taskListPage;

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}
