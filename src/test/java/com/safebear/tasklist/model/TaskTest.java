package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

/**
 * Created by cca_student on 11/04/2018.
 */

public class TaskTest {

    @Test
    public void creation(){
        LocalDate localDate = LocalDate.now();

        Task task = new Task(1L, "Configure Jenkins", localDate, false);

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

}
