Feature: Task Management

  User Story:
  In order to manage my tasks
  As a user
  I must be able to view, add, archive or update tasks

  Rules:
   - You must be able to view all tasks
   - They must be paginated
   - You must be able to see the name, due date and status of a task
   - You must be able to change the status of a task
   - You must be able to archive tasks

  Questions:
   - How many tasks per page for the pagination?
   - Where do you archive to?
   - Is the due date configurable?

  To do:
   - Pagination
   - Archiving

  Domain language:
   - Task
   - Due Date
   - Status
   - Task List

  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
    |task|
    |Create documentation|